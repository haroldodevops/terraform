resource "aws_ecr_repository" "this" {
  name = "${local.app-name}-repository"
}
#resource "null_resource" "docker" {
#  triggers = {
#    md5 = "${var.app_folder}"
#  }
#
#  provisioner "local-exec" {
#    working_dir = var.app_folder
#    command     = "cat ${aws_ecr_repository.this.repository_url}"
#  }
#
#  provisioner "local-exec" {
#    working_dir = var.app_folder
#    command     = "docker build -t ${local.app-name} ."
#  }
#
#  provisioner "local-exec" {
#    working_dir = var.app_folder
#    command     = "docker tag ${local.app-name}:latest ${aws_ecr_repository.this.repository_url}:latest"
#  }
#
#  provisioner "local-exec" {
#    working_dir = var.app_folder
#    command     = "docker push ${aws_ecr_repository.this.repository_url}:latest"
#  }
#}
