resource "aws_cloudwatch_log_group" "this" {
  name              = "/ecs/${local.app-name}"
  retention_in_days = var.log_retention

  tags = merge({ Name = "${local.app-name}-log-group" }, local.tags)
}




