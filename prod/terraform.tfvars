region          = "eu-east-1"
env             = "prod"
app_port        = 8080
app_count       = 3
fargate_cpu     = 1024
fargate_memory  = 2048
ac_min_capacity = 3
ac_max_capacity = 6
log_retentin    = 30
